<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/22/19
 * Time: 8:40 AM
 */

namespace App\Art\Models;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['name', 'artisan_id', 'description', 'price', 'quantity', 'category'];
}
