<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/22/19
 * Time: 8:52 AM
 */

namespace App\Http\Controllers;


use App\Art\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();

        return response()->json($products);
    }

    public function store(Request $request)
    {
        $product = Product::create($request->all());

        return response()
            ->json([
                'data' => $product
            ],Response::HTTP_CREATED);
    }

    public function show($product_id)
    {
        $product = Product::findOrFail($product_id);

        return response()->json($product);
    }

    public function update(Request $request, $product_id)
    {
        $product = Product::findOrFail($product_id);

        if ($product->isClean()) {
            return response()->json([
                'message' => 'At least one value must change'
            ], Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        $product->save();

        return response()->json($product);
    }

    public function destroy($product_id)
    {
        $product = Product::findOrFail($product_id);

        $product->delete();

        return response()->json($product);
    }
}
